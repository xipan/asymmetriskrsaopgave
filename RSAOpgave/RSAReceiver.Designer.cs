﻿namespace RSAOpgave
{
    partial class RSAReceiver
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.modulusBox = new System.Windows.Forms.TextBox();
            this.exponentBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.qBox = new System.Windows.Forms.TextBox();
            this.pBox = new System.Windows.Forms.TextBox();
            this.inverseQBox = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.dqBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.dpBox = new System.Windows.Forms.TextBox();
            this.dBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.cipherbytesBox = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.decryptedBox = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.modulusBox);
            this.groupBox1.Controls.Add(this.exponentBox);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("宋体", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox1.Location = new System.Drawing.Point(12, 25);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(544, 114);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Public Data";
            // 
            // modulusBox
            // 
            this.modulusBox.Location = new System.Drawing.Point(91, 75);
            this.modulusBox.Name = "modulusBox";
            this.modulusBox.Size = new System.Drawing.Size(434, 24);
            this.modulusBox.TabIndex = 4;
            // 
            // exponentBox
            // 
            this.exponentBox.Location = new System.Drawing.Point(91, 32);
            this.exponentBox.Name = "exponentBox";
            this.exponentBox.Size = new System.Drawing.Size(434, 24);
            this.exponentBox.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 78);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 15);
            this.label2.TabIndex = 2;
            this.label2.Text = "Modulus:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 15);
            this.label1.TabIndex = 1;
            this.label1.Text = "Exponent:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.qBox);
            this.groupBox2.Controls.Add(this.pBox);
            this.groupBox2.Controls.Add(this.inverseQBox);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.dqBox);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.dpBox);
            this.groupBox2.Controls.Add(this.dBox);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Font = new System.Drawing.Font("宋体", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox2.Location = new System.Drawing.Point(12, 145);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(544, 218);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Private Data";
            // 
            // qBox
            // 
            this.qBox.Location = new System.Drawing.Point(91, 185);
            this.qBox.Name = "qBox";
            this.qBox.Size = new System.Drawing.Size(434, 24);
            this.qBox.TabIndex = 12;
            // 
            // pBox
            // 
            this.pBox.Location = new System.Drawing.Point(91, 155);
            this.pBox.Name = "pBox";
            this.pBox.Size = new System.Drawing.Size(434, 24);
            this.pBox.TabIndex = 11;
            // 
            // inverseQBox
            // 
            this.inverseQBox.Location = new System.Drawing.Point(91, 122);
            this.inverseQBox.Name = "inverseQBox";
            this.inverseQBox.Size = new System.Drawing.Size(434, 24);
            this.inverseQBox.TabIndex = 10;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 179);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(23, 15);
            this.label8.TabIndex = 9;
            this.label8.Text = "Q:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 151);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(23, 15);
            this.label7.TabIndex = 8;
            this.label7.Text = "P:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 125);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(79, 15);
            this.label6.TabIndex = 7;
            this.label6.Text = "InverseQ:";
            // 
            // dqBox
            // 
            this.dqBox.Location = new System.Drawing.Point(91, 92);
            this.dqBox.Name = "dqBox";
            this.dqBox.Size = new System.Drawing.Size(434, 24);
            this.dqBox.TabIndex = 6;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 95);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(31, 15);
            this.label5.TabIndex = 5;
            this.label5.Text = "DQ:";
            // 
            // dpBox
            // 
            this.dpBox.Location = new System.Drawing.Point(91, 62);
            this.dpBox.Name = "dpBox";
            this.dpBox.Size = new System.Drawing.Size(434, 24);
            this.dpBox.TabIndex = 4;
            // 
            // dBox
            // 
            this.dBox.Location = new System.Drawing.Point(91, 32);
            this.dBox.Name = "dBox";
            this.dBox.Size = new System.Drawing.Size(434, 24);
            this.dBox.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 65);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 15);
            this.label3.TabIndex = 2;
            this.label3.Text = "DP:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 35);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(23, 15);
            this.label4.TabIndex = 1;
            this.label4.Text = "D:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("宋体", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label9.Location = new System.Drawing.Point(17, 375);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(103, 15);
            this.label9.TabIndex = 8;
            this.label9.Text = "Cipherbytes:";
            // 
            // cipherbytesBox
            // 
            this.cipherbytesBox.Location = new System.Drawing.Point(126, 375);
            this.cipherbytesBox.Name = "cipherbytesBox";
            this.cipherbytesBox.Size = new System.Drawing.Size(411, 21);
            this.cipherbytesBox.TabIndex = 13;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(241, 406);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 14;
            this.button1.Text = "Decrypt";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("宋体", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label10.Location = new System.Drawing.Point(17, 435);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(87, 15);
            this.label10.TabIndex = 15;
            this.label10.Text = "Decrypted:";
            // 
            // decryptedBox
            // 
            this.decryptedBox.Location = new System.Drawing.Point(103, 435);
            this.decryptedBox.Name = "decryptedBox";
            this.decryptedBox.Size = new System.Drawing.Size(434, 21);
            this.decryptedBox.TabIndex = 16;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("宋体", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label11.Location = new System.Drawing.Point(18, 476);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(239, 15);
            this.label11.TabIndex = 24;
            this.label11.Text = "Time / message at decryption:";
            // 
            // RSAReceiver
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(568, 511);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.decryptedBox);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.cipherbytesBox);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "RSAReceiver";
            this.Text = "RSAReceiver";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox modulusBox;
        private System.Windows.Forms.TextBox exponentBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox qBox;
        private System.Windows.Forms.TextBox pBox;
        private System.Windows.Forms.TextBox inverseQBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox dqBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox dpBox;
        private System.Windows.Forms.TextBox dBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox cipherbytesBox;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox decryptedBox;
        private System.Windows.Forms.Label label11;
    }
}