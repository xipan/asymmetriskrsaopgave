﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RSAOpgave
{
    public partial class RSAReceiver : Form
    {
        private RSATool _rsa;

        public RSAReceiver()
        {
            InitializeComponent();
            RSATool rsaHandler = new RSATool("Key_Container_Name", 1024);
            RSACryptoServiceProvider rsa = rsaHandler.GetRSAHandler;
            //Load private key
            RSAParameters rsaParams = rsa.ExportParameters(true);            
            this.exponentBox.Text = BitConverter.ToString(rsaParams.Exponent);
            this.modulusBox.Text = BitConverter.ToString(rsaParams.Modulus);
            this.dBox.Text = BitConverter.ToString(rsaParams.D);
            this.dpBox.Text = BitConverter.ToString(rsaParams.DP);
            this.dqBox.Text = BitConverter.ToString(rsaParams.DQ);
            this.inverseQBox.Text = BitConverter.ToString(rsaParams.InverseQ);
            this.pBox.Text = BitConverter.ToString(rsaParams.P);
            this.qBox.Text = BitConverter.ToString(rsaParams.Q);

            //String[] arr = this.modulusBox.Text.Split('-');
            //byte[] array = new byte[arr.Length];
            //for (int i = 0; i < arr.Length; i++) 
            //    array[i] = Convert.ToByte(arr[i], 16);
            //string str = Convert.ToBase64String(array);

            _rsa = rsaHandler;
            string publicKey1 = rsa.ToXmlString(false);
            string privateKey1 = rsa.ToXmlString(true);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Stopwatch sw = new Stopwatch();

            sw.Start();
            this.decryptedBox.Text = _rsa.Decrypt(this.cipherbytesBox.Text.Trim());
            sw.Stop();
            
            //view time
            double ticks = sw.ElapsedTicks;
            double seconds = ticks / Stopwatch.Frequency;
            this.label11.Text = string.Format("Time / message at decryption: {0} seconds", seconds.ToString());


        }
    }
}
