﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace RSAOpgave
{
    class RSATool
    {
        private RSACryptoServiceProvider _rsa;
        private CspParameters _cspParams;
        public RSACryptoServiceProvider GetRSAHandler { get; set; }

        public RSATool(string containerName, int dwkeySize)
        {            
            _cspParams = new CspParameters { KeyContainerName = containerName };
            _rsa = new RSACryptoServiceProvider(dwkeySize, _cspParams);
            this.GetRSAHandler = _rsa;
        }

        public string Decrypt(string strEntryText)
        {
            byte[] byteEntry = Convert.FromBase64String(strEntryText);
            byte[] byteText = _rsa.Decrypt(byteEntry, false);
            return Encoding.UTF8.GetString(byteText);
        }

    }

    class RSAToolWithPublicKey
    {
        private RSACryptoServiceProvider _rsa;
        public RSACryptoServiceProvider GetRSAHandler { get; set; }

        public RSAToolWithPublicKey(string publicKeyString, int dwkeySize)
        {
            _rsa = new RSACryptoServiceProvider(dwkeySize);
            _rsa.FromXmlString(publicKeyString.ToString());
            GetRSAHandler = _rsa;
        }

        public string Encrypt(string strText)
        {
            byte[] byteText = Encoding.UTF8.GetBytes(strText);
            byte[] byteEntry = _rsa.Encrypt(byteText, false);
            return Convert.ToBase64String(byteEntry);
        }


    }
}
