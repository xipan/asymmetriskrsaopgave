﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RSAOpgave
{
    public partial class RSASender : Form
    {
        public RSASender()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Get modulus of Key as string
            String[] arr = this.modulusBox.Text.Split('-');
            byte[] array = new byte[arr.Length];
            for (int i = 0; i < arr.Length; i++) 
                array[i] = Convert.ToByte(arr[i], 16);
            string modulusStr = Convert.ToBase64String(array);

            //Get exponent of Key as string
            arr = this.exponentBox.Text.Split('-');
            array = new byte[arr.Length];
            for (int i = 0; i < arr.Length; i++)
                array[i] = Convert.ToByte(arr[i], 16);
            string exponentStr = Convert.ToBase64String(array);
            string publicKey = string.Format("<RSAKeyValue><Modulus>{0}</Modulus><Exponent>{1}</Exponent></RSAKeyValue>", modulusStr, exponentStr);

            RSAToolWithPublicKey rsaHandler = new RSAToolWithPublicKey(publicKey,1024);

            Stopwatch sw = new Stopwatch();
            sw.Start();

            this.cipherbytesBox.Text = rsaHandler.Encrypt(this.plaintextBox.Text.Trim());

            sw.Stop();

            //view time
            double ticks = sw.ElapsedTicks;
            double seconds = ticks / Stopwatch.Frequency;            
            this.label3.Text = string.Format("Time / message at encryption: {0} seconds", seconds.ToString());



        }

        private void RSASender_Load(object sender, EventArgs e)
        {

        }
    }
}
